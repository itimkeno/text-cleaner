#pragma once

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

struct callback
{
    callback() = default;
    ~callback();
    void save_deleted_words(const std::string& word);

private:
    std::stringstream deleted_words;
};

struct contains_numbers_only
{
    bool operator()(const std::string& word) const;
};

struct contains_uppercase_only
{
    bool operator()(const std::string& word) const;
};

struct contains_uppercase_apostr_only
{
    bool operator()(const std::string& word) const;
};

struct contains_lowercase
{
    bool operator()(const std::string& word) const;
};