#include "support.hxx"
#include "text_transform.hxx"

#include <iostream>
#include <string>

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        std::cerr << "Wrong arguments count. We want only one argument: file "
                     "name (any_file.txt).\n";
        return EXIT_FAILURE;
    }

    const std::string file_name = argv[1];

    bool is_success = delete_words_from_file(
        file_name, contains_uppercase_apostr_only(), callback());

    return is_success;
}