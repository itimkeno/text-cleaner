#include "support.hxx"

callback::~callback()
{
    if (deleted_words.tellp() != std::streampos(0))
    {
        std::ofstream out_file("deleted_words.txt", std::ios_base::out);

        if (out_file.is_open())
            out_file << deleted_words.str();
        else
            std::cerr << "Can't save callback result.\n";

        out_file.close();
    }
}

void callback::save_deleted_words(const std::string& word)
{
    deleted_words << word << ' ';
}

bool contains_numbers_only::operator()(const std::string& word) const
{
    for (size_t i = 0; i < word.size(); ++i)
    {
        if (!isdigit(word[i]))
            return false;
    }
    return true;
}

bool contains_uppercase_only::operator()(const std::string& word) const
{
    for (size_t i = 0; i < word.size(); ++i)
    {
        if (!isupper(word[i]))
            return false;
    }
    return true;
}

bool contains_uppercase_apostr_only::operator()(const std::string& word) const
{
    for (size_t i = 0; i < word.size(); ++i)
    {
        if (word[i] == '\'')
            continue;

        if (!isupper(word[i]))
            return false;
    }
    return true;
}

bool contains_lowercase::operator()(const std::string& word) const
{
    for (size_t i = 0; i < word.size(); ++i)
    {
        if (islower(word[i]))
            return true;
    }
    return false;
}
