#include "support.hxx"

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

TEST_CASE("Word contains only numbers == true", "[contains_numbers_only]")
{
    REQUIRE(contains_numbers_only()("2357") == true);
    REQUIRE(contains_numbers_only()("5kg") == false);
    REQUIRE(contains_numbers_only()("lov3") == false);
}

TEST_CASE("Word contains only uppercase == true", "[contains_uppercase_only]")
{
    REQUIRE(contains_uppercase_only()("UPPERCASE") == true);

    REQUIRE(contains_uppercase_only()("SHE'LL") == false);
    REQUIRE(contains_uppercase_only()("Don't") == false);
    REQUIRE(contains_uppercase_only()("DiFfErEnT") == false);
    REQUIRE(contains_uppercase_only()("camelCase") == false);
    REQUIRE(contains_uppercase_only()("lowercase") == false);
}

TEST_CASE("Word contains only uppercase and apostrophe == true",
          "[contains_uppercase_apostr_only]")
{
    REQUIRE(contains_uppercase_apostr_only()("UPPERCASE") == true);
    REQUIRE(contains_uppercase_apostr_only()("SHE'LL") == true);

    REQUIRE(contains_uppercase_apostr_only()("Don't") == false);
    REQUIRE(contains_uppercase_apostr_only()("DiFfErEnT") == false);
    REQUIRE(contains_uppercase_apostr_only()("camelCase") == false);
    REQUIRE(contains_uppercase_apostr_only()("lowercase") == false);
}

TEST_CASE("Word contains one or more lowercase == true", "[contains_lowercase]")
{
    REQUIRE(contains_lowercase()("UPPERCASE") == false);
    REQUIRE(contains_lowercase()("SHE'LL") == false);

    REQUIRE(contains_lowercase()("Don't") == true);
    REQUIRE(contains_lowercase()("DiFfErEnT") == true);
    REQUIRE(contains_lowercase()("camelCase") == true);
    REQUIRE(contains_lowercase()("lowercase") == true);
}
