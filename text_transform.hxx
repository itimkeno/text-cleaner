#pragma once

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

// file_name - path to file
// T predicate - must contain overloading bool operator()(std::string)
// callback - keeps words when predicate is true
template <typename T>
bool delete_words_from_file(const std::string& file_name, T predicate,
                            callback words_keeper)
{
    std::stringstream file_in_memory;
    std::ifstream     in_file(file_name, std::fstream::in);

    if (!in_file.is_open())
    {
        std::cerr << "Can't read from source file.\nCan't open " + file_name
                  << std::endl;
        return EXIT_FAILURE;
    }

    // if we don't want words with apostrophe, simply add "\'" to separator
    std::string separator{ "\t\n\r.,:;!?/\\ \"|@#$%^&*(){}[]<>" };
    std::string line;
    std::string word;
    // read source text from file
    while (std::getline(in_file, line))
    {
        std::string reduced_line;
        size_t      begin_word = 0;
        size_t      end_word   = 0;
        size_t      begin_line = 0;
        size_t      end_line   = 0;

        do
        {
            begin_word = line.find_first_not_of(separator, begin_word);
            if (begin_word == std::string::npos)
            {
                reduced_line +=
                    line.substr(begin_line, line.size() - begin_line);
                break;
            }
            end_word = line.find_first_of(separator, begin_word);
            word     = line.substr(begin_word, end_word - begin_word);

            end_line = begin_word;
            reduced_line += line.substr(begin_line, end_line - begin_line);

            if (predicate(word))
            {
                words_keeper.save_deleted_words(word);
                begin_line = end_word;
            }

            if (!predicate(word))
            {
                begin_line = begin_word;
                end_line   = end_word;
                reduced_line += line.substr(begin_line, end_line - begin_line);
                begin_line = end_line;
            }

            begin_word = end_word + 1;
        } while (end_word != std::string::npos);

        file_in_memory << reduced_line << '\n';
    }
    in_file.close();

    // write edited text to file
    std::ofstream out_file(file_name, std::ios_base::out);

    if (!out_file.is_open())
    {
        std::cerr << "Can't open and write to " + file_name << std::endl;
        return EXIT_FAILURE;
    }

    out_file << file_in_memory.str();
    out_file.close();

    return EXIT_SUCCESS;
}