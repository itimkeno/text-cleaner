# text-cleaner

 This project uses [Catch2] framework.
  
 Deleted words was saved in deleted_words.txt

# How to build

  You can simply open this project as a folder in any IDE with built-in CMake support. 
  
  Or with terminal:
```
  mkdir build
  cd build
  cmake ../ 
  cmake --build .
```
  
# Run 

```
  ./task-bin "your_text_file"
``` 

# Run tests

```
  ./unit-tests
```

[Catch2]: https://github.com/catchorg/Catch2/tree/v2.x
